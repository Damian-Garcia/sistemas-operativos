#define _GNU_SOURCE
#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define NTHREADS 4

long long num_steps = 1000000000;
double step;

//Se agrega una variable para acumular los resultados del tfunc
int tfunc(void *args, double *res)
{
    int i;
    double x;
    double sum = 0.0;
    double tSum;

    int tnum = *((int *)args);
    int rinic = tnum * (num_steps / NTHREADS);
    int rfin = (tnum + 1) * (num_steps / NTHREADS);

    for (i = rinic; i < rfin; i++)
    {
        x = (i + .5) * step;
        tSum = tSum + 4.0 / (1. + x * x);
    }

    sum = sum + tSum;
    *res = sum;
    exit(0);
}

int main()
{
    long long start_ts;
    long long stop_ts;
    float elapsed_time;
    struct timeval ts;
    double pi = 0.0;
    int i;

    int idCompartida;
    double *punteroCompartida;

    //Se obtiene un id para acceder a la memoria compartida
    idCompartida = shmget(IPC_PRIVATE, 4 * sizeof(double), IPC_CREAT | 0666);
    if (idCompartida < 0)
    {
        printf("Problemas al solicitar memoria compartida");
        exit(1);
    }

    //Se recupera la memoria con el id
    punteroCompartida = (double *)shmat(idCompartida, NULL, 0);
    if ((int)punteroCompartida == -1)
    {
        printf("Problemas al utilizar la memoria compartida");
        exit(1);
    }

    int pid[NTHREADS];
    int status[NTHREADS];

    int numID[NTHREADS];

    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec * 1000000 + ts.tv_usec; // Tiempo inicial
    step = 1. / (double)num_steps;

    for (i = 0; i < NTHREADS; i++)
    {
        numID[i] = i;
        // Se hace el fork para crear los procesos hijos
        pid[i] = fork();
        if (pid[i] == 0)
        { // En el hijo se manda el id y su espacio de memoria compartida
            tfunc((void *)&numID[i], &punteroCompartida[i]);
        }
    }

    for (i = 0; i < NTHREADS; i++)
    {
        if (pid[i] < 0)
            printf("Problemas al crear los hilos\n");
    }
    for (i = 0; i < NTHREADS; i++)
    {
        //Se espera a que los procesos hijos terminen
        wait(&status[i]);
    }
    double sum = 0.0;
    for (i = 0; i < NTHREADS; i++)
    {
        sum += punteroCompartida[i];
    }

    pi = sum * step;
    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec * 1000000 + ts.tv_usec; // Tiempo final
    elapsed_time = (float)(stop_ts - start_ts) / 1000000.0;
    printf("El valor de PI es %1.12f\n", pi);
    printf("Tiempo = %2.2f segundos\n", elapsed_time);
    return 0;
}
