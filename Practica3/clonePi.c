#define _GNU_SOURCE
#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/wait.h>
#include <unistd.h>

#define NTHREADS 4
#define  STACK_SIZE (1024 * 1024) //Se define el tamano del stack de los hilos
long long num_steps = 1000000000;
double step;
double sum=0.0;

int tfunc(void *args)
{
  int i;
  double x;
  double tSum;

  int tnum=*((int *) args);
  int rinic=tnum*(num_steps/NTHREADS);
  int rfin=(tnum+1)*(num_steps/NTHREADS);

  for (i = rinic; i < rfin; i++)
  {
    x = (i + .5)*step;
    tSum = tSum + 4.0/(1.+ x*x);
  }

  sum = sum + tSum;

  return 0;
}

int main()
{
  long long start_ts;
  long long stop_ts;
  float elapsed_time;
  struct timeval ts;
  double pi=0.0;
  int i;

  //Clone
  char *stack[NTHREADS];
  int pid[NTHREADS];
  int status[NTHREADS];

  int numID[NTHREADS];

  gettimeofday(&ts, NULL);
  start_ts = ts.tv_sec * 1000000 + ts.tv_usec; // Tiempo inicial
  step = 1./(double)num_steps;

  for(i=0;i<NTHREADS;i++)
  {
    //Se le asigna la memeoria a cada hilo
    stack[i] = malloc(STACK_SIZE);
    numID[i]=i;
    // Se crean los hilos con el tamano del stack indicado y se obtiene su id
    // Las banderas que se le pasan a la funcion son CLONE_VM para que corra en el mismo espacio de memoria
    // Y SIGCHILD es la senal que se se va a mandar cuando el proceso
    pid[i] = clone(tfunc, stack[i] + STACK_SIZE, CLONE_VM |SIGCHLD,(void *) &numID[i]);
  }

    for(i=0;i<NTHREADS;i++)
    {
      if(pid[i] < 0)
        printf("Problemas al crear los hilos\n");
    }
    for(i=0;i<NTHREADS;i++)
    {
      //Se espera a que los hijos terminen
      wait(&status[i]);
    }
    for(i=0;i<NTHREADS;i++)
    {
      //Se libera la memoria solicitada para los hijos
      free(stack[i]);
    }

    pi = sum*step;
    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec * 1000000 + ts.tv_usec; // Tiempo final
    elapsed_time = (float) (stop_ts - start_ts)/1000000.0;
    printf("El valor de PI es %1.12f\n",pi);
    printf("Tiempo = %2.2f segundos\n",elapsed_time);
    return 0;
}
