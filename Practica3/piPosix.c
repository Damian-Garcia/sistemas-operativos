#include <stdio.h>
#include <sys/time.h>
#include <pthread.h>

#define NTHREADS 4
long long num_steps = 1000000000;
double step,  sum=0.0;

void *tfunc(void *args)
{
  int i;
  double x;
  double tSum;

  int tnum=*((int *) args);
	int rinic=tnum*(num_steps/NTHREADS);
	int rfin=(tnum+1)*(num_steps/NTHREADS);

  for (i = rinic; i < rfin; i++)
  {
    x = (i + .5)*step;
    tSum = tSum + 4.0/(1.+ x*x);
  }

  sum = sum + tSum;
  return 0;
}

int main()
{
  long long start_ts;
  long long stop_ts;
  float elapsed_time;
  struct timeval ts;
  double pi=0.0;
  int i;

  // Se creo un arreglo de pthread_t, para guardar los identificadores de los hilos
  pthread_t idhilo[NTHREADS];
  int numID[NTHREADS];

  gettimeofday(&ts, NULL);
  start_ts = ts.tv_sec * 1000000 + ts.tv_usec; // Tiempo inicial
  step = 1./(double)num_steps;

  for(i=0;i<NTHREADS;i++)
  {
    numID[i]=i;
    // Se crea el hilo con su correspondiente id y se manda el numero de hilo que es para el algoritmo
    pthread_create(&idhilo[i],NULL,tfunc,(void *) &numID[i]);
  }

  for(i=0;i<NTHREADS;i++)
    // Se espera a que los hilos terminen
    pthread_join(idhilo[i],NULL);

    pi = sum*step;
    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec * 1000000 + ts.tv_usec; // Tiempo final
    elapsed_time = (float) (stop_ts - start_ts)/1000000.0;
    printf("El valor de PI es %1.12f\n",pi);
    printf("Tiempo = %2.2f segundos\n",elapsed_time);
    return 0;
}
