#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

/*
*Función para cambiar las variables de la cadena por su valor.
*/
char * QuitarVariables(char *cadenaI)
{
  char *cadenaN = malloc(50);
  char *cadenaS = malloc(50);
  char *us;
  int flag_s = 0;
  int cont = 0;
  int offset = 0;
  //for donde se va
  int i ;
  for( i = 0; strlen(cadenaI) > i; i++)
  {
    if(flag_s)
    {
      offset++;
      if(cadenaI[i] == ' ' || cadenaI[i] == '\n')
      {
        us = getenv(cadenaN);
        if(us == NULL)
        {
          strcat(cadenaS, "\r");
        }
        else
        {
          strcat(cadenaS, us);
        }
        cont = 0;
        flag_s = 0;
        memset(cadenaN,0,strlen(cadenaN));
      }
      else
      {
        cadenaN[cont] = cadenaI[i];
        cont++;
      }
    }
    else if(cadenaI[i] == '$')
    {
      flag_s = 1;
    }
    else
    {
      //Se pasa la cadena anterior a la nueva cadena
      cadenaS[i+offset] = cadenaI[i];
    }
  }
  return cadenaS;
}
/*
*Función para separar la cadena en variables.
*/
char ** SepararPalabras(char *cadena, int * cantidadPalabras)
{
  char *token = malloc(50);
  char **cadenaN = malloc(sizeof(char *) * 40);
  char sep[] = " \n";
  int cont = 0;
  token = strtok(cadena, sep);
  while( token != NULL )
  {
    cadenaN[cont] = token;
    token = strtok( NULL, sep);

    cont++;
  }
  // cadenaN[cont] = NULL;
  *cantidadPalabras = cont;
  int i = 0;
  return cadenaN;
}

int terminaConAmp(char ** cadena, int numPalabras){
  int longitud = strlen(cadena[numPalabras - 1]);
  return cadena[numPalabras - 1][longitud - 1] == '&';
}
/*
*
*/
int shell(char ** cadena, int longitud_palabras){
  int deco = -1;
  int pid;
  int longitud;
  char * palabraI = cadena[0];
  char *variable = cadena[1];
  char *rutaActual;
  int status;
  //Serie de condicionales para ver a que estado del switch case se va a entrar.
  if(strcmp(palabraI, "echo") == 0)
  {
    deco = 1;
  }
  else if(strcmp(palabraI, "exit") == 0)
  {
    deco = 2;
  }
  else if(strcmp(palabraI, "shutdown") == 0)
  {
    deco = 3;
  }
  else if(strcmp(palabraI, "export") == 0)
  {
    deco = 4;
  }
//Switch case para ejecutar los comandos que correspondan.

  switch (deco)
  {
  //En este caso se imprime la variable que se mando con el comando echo
    case 1:
      printf("%s\n", variable);
      break;
  //En este caso se sale de sh y va a getty
    case 2:
      exit(0);
      break;
  //En esta condicional se sale de todo
    case 3:
      exit(1);
      break;
    case 4:
  //En esta condicional se guarda la variable y su valor.
      putenv(variable);
      break;
  //Se va a entrar aqui cuando sea otro comando
    default:


      if( terminaConAmp(cadena, longitud_palabras))
      {
        longitud = strlen(cadena[longitud_palabras - 1]);
        if( longitud == 1 )
        {
          cadena[longitud_palabras - 1] = NULL;
        }
        else{
          cadena[longitud_palabras - 1][longitud - 1] = '\0';
        }
        pid = fork();
        if(0 == pid)
          execvp(cadena[0], cadena);

      }
      else
      {
        int res = 0;
        pid = fork();
        if(0 == pid){
          res = execvp(cadena[0], cadena);
          if(res == -1)
          printf("El comando no existe\n");
            exit(EXIT_FAILURE);
        }
        wait(&status);
      }
      break;
    }
}



int main()
{
  char *cadena_teclado;
  cadena_teclado = malloc(50);
  char * cadena_sustituida;
  char ** palabras;
  int salir = 0;
  for(;;)
  {
    int numParametros;
    //QuitarVariables("echo $variable $nepe ");
    printf("s > ");
    fgets(cadena_teclado, 50, stdin);
    cadena_sustituida = QuitarVariables(cadena_teclado); //Se obtiene la nueva cadena
    palabras = SepararPalabras(cadena_sustituida, &numParametros);
    shell(palabras, numParametros);
  }

}
