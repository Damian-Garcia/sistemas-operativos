#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>

int main()
{
	int pid;
	int i = 0;
	int status;
	int cont = 0;
	int cantidad_usuarios;
	int usuario_contrasena_correctos = 0;

	char user[50];
	char pass[50];
	char file_user[50][50];
	char file_pass[50][50];
	char archivo[100];
	FILE *arch;
	arch = fopen("passwd.txt", "r");
	int fichero;

	if(arch == NULL)
	{
		printf("No hay archivo de usuarios\n" );
		mode_t modo = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
		char *nombre = "archivo";
		fichero = open(nombre, O_WRONLY | O_CREAT | O_TRUNC, modo);
		sleep(1);
		exit(1);

	}
	printf("%s\n", file_user[0]);
	while(feof(arch) == 0)
	{

		fgets(archivo,100,arch);
		sscanf(archivo, "%[^:]:%s", file_user[cont], file_pass[cont]);
		cont++;
	}
	cantidad_usuarios = cont - 1;
	fclose(arch);
	while (status != 256)
	{
		printf("Usuario: ");
		scanf("%s", &user);
		printf("Contraseña: ");
		scanf("%s", &pass);
		for(cont = 0; cont < cantidad_usuarios; cont++)
		{
			if (strcmp(user, file_user[cont]) == 0 || strcmp(pass, file_pass[cont]) == 0)
				usuario_contrasena_correctos = 1;
		}
		if (usuario_contrasena_correctos == 0)
		{
			printf("Usuario o contraseña incorrectos\n");
		}
		else
		{
			pid = fork();
			if (pid == 0)
				execlp("./sh", NULL);
			wait(&status);
		}
	}
	mode_t modo = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	char *nombre = "archivo";
	fichero = open(nombre, O_WRONLY | O_CREAT | O_TRUNC, modo);
	//close(fd);

	exit(1);
}
