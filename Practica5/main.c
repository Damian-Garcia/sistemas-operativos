#include <stdio.h>
#include <stdlib.h>
#include "Semaforos.h"
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define CICLOS 10

Semaforo * sem;
char *pais[3]={"Peru","Bolvia","Colombia"};

void proceso(int i)
{
    int k;
    for (k = 0; k < CICLOS; k++)
    {
        waitsem(sem);
        // Llamada waitsem implementada en la parte 3
        printf("Entra %s ", pais[i]);
        fflush(stdout);
        sleep(rand() % 3);
        printf("- %s Sale\n", pais[i]);
        // Llamada waitsignal implementada en la parte 3
        signalsem(sem);
        // Espera aleatoria fuera de la sección crítica
        sleep(rand() % 3);

    }
    exit(0); // Termina el proceso
}

// Incializar el contador del semáforo en 1 una vez que esté
// en memoria compartida, de manera que solo a un proceso se le
// permitirá entrar a la sección crítica
int *g, *h, *k, *j;


int main(int argc, char *argv[])
{
    int shmid3=shmget(IPC_PRIVATE,sizeof(Semaforo), 0666|IPC_CREAT);
    sem =(Semaforo * )shmat(shmid3,NULL,0);

    int i ;

    int shmid=shmget(IPC_PRIVATE,sizeof(int )*4, 0666|IPC_CREAT);
    g=(int * )shmat(shmid,NULL,0);

    int shmid2=shmget(IPC_PRIVATE,sizeof(int )*4, 0666|IPC_CREAT);
    h=(int * )shmat(shmid2,NULL,0);

    int shmid4=shmget(IPC_PRIVATE,sizeof(int )*4, 0666|IPC_CREAT);
    j=(int * )shmat(shmid4,NULL,0);

    *g = 0;
    *h = 0;
    *j = 0;
    initsem(&sem,1);

    for(i = 0 ; i < 3; i++){
        int res;
        res = fork();
        if(res == 0){
            proceso(i);
        }
    }
    int status;
    for(i = 0 ; i < 3 ; i++){
        wait(&status);
    }

    return 0;
}
