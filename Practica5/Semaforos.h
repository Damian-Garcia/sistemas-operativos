#include <unistd.h>
#include <sys/types.h>   

#define MAXTHREAD 10

typedef struct _QUEUE {
	pid_t  elements[MAXTHREAD];
	int head;
	int tail;
} QUEUE;

typedef struct _Sem{
    int valor;
    QUEUE espera; 
} Semaforo; 


void waitsem(Semaforo * s);
void signalsem(Semaforo * s);
void initsem(Semaforo ** s, int value);
	