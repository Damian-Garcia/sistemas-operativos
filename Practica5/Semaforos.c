#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>

#include "Semaforos.h"

#define atomic_xchg(A,B) __asm__ __volatile__( \
	" lock xchg %1,%0 ;\n" \
	: "=ir" (A) \
	: "m" (B), "ir" (A) \
	);

extern int *g;//Variabble utilizado para la función de waitsem
extern int *h;//Variable utilizada para la función de signalsem
extern int *j;//Variable utilizada para la función de initsem


void _enqueue(QUEUE *q,pid_t val);
int _dequeue(QUEUE *q);
int _emptyq(QUEUE *q);

void waitsem(Semaforo * s){
	int l = 1;
	do { atomic_xchg(l,*g); } while(l!=0);
	s->valor--;
    if(s->valor<0){
        pid_t id = getpid();
		_enqueue(&s->espera, id);
		//puts("dormmir");
		kill(id, SIGSTOP);

	}

	*g = 0;

}

void signalsem(Semaforo * s){
	int l = 1;
	do { atomic_xchg(l,*h); } while(l!=0);
	s->valor++;


	if(s->valor <=0 ){
		pid_t t = _dequeue(&(s->espera));
		//puts("despertar");
		kill(t, SIGCONT);
	}
	*h = 0;
}

void initsem(Semaforo ** s, int value){
	//*s = malloc(sizeof(Semaforo));
		int l  = 1;
		do { atomic_xchg(l,*j); } while(l!=0);
    (*s)->valor = value;
		*j = 0;
}

void _enqueue(QUEUE *q,pid_t val)
{
	q->elements[q->head]=val;
	// Incrementa al apuntador
	q->head++;
	q->head=q->head%MAXTHREAD;
}


int _dequeue(QUEUE *q)
{
	int valret;
	valret=q->elements[q->tail];
	// Incrementa al apuntador
	q->tail++;
	q->tail=q->tail%MAXTHREAD;
	return(valret);
}

int _emptyq(QUEUE *q)
{
	return(q->head==q->tail);
}
