#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>


#include "semaphores.h"

#define CICLOS 10
char *pais[3]={"Peru","Bolvia","Colombia"};

int *g;
int sid;//Variable del semaforo

void proceso(int i)
{
  int k;
  int l;
  for(k=0;k<CICLOS;k++)
  {
  // Entrada a la sección crítica
  semWait(sid);
  printf("Entra %s",pais[i]);
  fflush(stdout);
  sleep(rand()%3);
  printf("- %s Sale\n",pais[i]);
  // Salida de la sección crítica
  semSignal(sid);
  // Espera aleatoria fuera de la sección crítica
  sleep(rand()%3);
  }
  exit(0);
  // Termina el proceso
}

int main()
{
  if((sid = semget( IPC_PRIVATE, 1, IPC_CREAT | 0660)) == -1)
   {
      return(-1);
   }

  semInit(sid, 1);

  int pid;
  int status;
  int args[3];
  int i;
  srand(getpid());

  for(i=0;i<3;i++)
  {
  // Crea un nuevo proceso hijo que ejecuta la función proceso()
  pid=fork();
  if(pid==0)
  proceso(i);

  }
  for(i=0;i<3;i++)
  pid = wait(&status);

}
