#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "semaphores.h"

void semInit(int idsem, int val)
{
  semctl(idsem, 0, SETVAL, val);
}

void semWait(int idsem)
{
  struct sembuf s;
  	s.sem_num=0; // Primero y único elemento del semáforo
  	s.sem_op=-1;
  	s.sem_flg=SEM_UNDO;

  	semop(idsem,&s,1);
  	return;
}

void semSignal(int idsem)
{
    struct sembuf s;
    s.sem_num=0; // Primero y único elemento del semáforo
    s.sem_op=1;
    s.sem_flg=SEM_UNDO;

    semop(idsem,&s,1);
    return;
}
