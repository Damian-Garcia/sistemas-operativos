#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <sys/msg.h>

#define CICLOS 10
char *pais[3]={"Peru","Bolvia","Colombia"};

int *g;
int qid;
//estructura dela queue
typedef struct mymsgbuf{
  int mtype;
}msg;

void proceso(int i)
{
  int k;
  int l;
  msg send;
  msg recive;

  for(k=0;k<CICLOS;k++)
  {
  // Entrada a la sección crítica
  msgrcv(qid,&recive, sizeof(int), 1,0);
  printf("Entra %s",pais[i]);
  fflush(stdout);
  sleep(rand()%3);
  printf("- %s Sale\n",pais[i]);
  // Salida de la sección crítica
  send.mtype = 1;
  msgsnd(qid, &send, sizeof(int), IPC_NOWAIT);
  // Espera aleatoria fuera de la sección crítica
  sleep(rand()%3);
  }
  exit(0);
  // Termina el proceso
}

int main()
{
  int pid;
  int status;
  int args[3];
  int i;
  srand(getpid());
  msg send;
  send.mtype = 1;
  //Se va a crear la queue
  if((qid = msgget( IPC_PRIVATE, IPC_CREAT | 0600 )) == -1)
  {
    return(-1);
  }

  msgsnd(qid, &send, sizeof(int), IPC_NOWAIT);

  for(i=0;i<3;i++)
  {
  // Crea un nuevo proceso hijo que ejecuta la función proceso()
  pid=fork();
  if(pid==0)
  proceso(i);

  }
  for(i=0;i<3;i++)
  pid = wait(&status);

}
