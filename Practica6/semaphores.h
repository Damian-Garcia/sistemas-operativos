#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void semInit(int key, int val);
void semWait(int idsem);
void semSignal(int idsem);
