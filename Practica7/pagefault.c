#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "mmu.h"

#define RESIDENTSETSIZE 3

extern char *base;
extern int framesbegin;
extern int idproc;
extern int systemframetablesize;
extern int ptlr;

extern struct SYSTEMFRAMETABLE *systemframetable;
extern struct PROCESSPAGETABLE *ptbr;
extern struct PROCESSPAGETABLE *gprocesspagetable;


int getfreeframe();

void isVirtualFrame(unsigned int frame);
void getFirstFreeFrame();
void swapPages(int frame);
void writeFile(int framenumber, char *buffer);
void readFile(int framenumber, char *buffer);

// Rutina de fallos de página

int pagefault(char *vaddress)
{
    int i;
    int frame;
    long pag_a_expulsar;
    long pag_del_proceso;

    // Calcula la página del proceso
    pag_del_proceso=(long) vaddress>>12;
    // Cuenta los marcos asignados al proceso
    i=countframesassigned();
  
    if( (i+1) > (RESIDENTSETSIZE)){
        swapPages(i+1);
    }

    // Busca un marco libre en el sistema
    frame=getfreeframe();
    (ptbr+pag_del_proceso)->modificado=0;

    if(frame==-1)
    {
        return(-1); // Regresar indicando error de memoria insuficiente
    }


    (ptbr+pag_del_proceso)->presente=1;
    (ptbr+pag_del_proceso)->framenumber=frame;


    return(1); // Regresar todo bien
}

void swapPages(int frame){
    int i, last = -1;
    int timeLastUsed = INT_MAX;
    for (i = 0; i < ptlr; i++)
    {
        if (ptbr[i].presente)
        {
            last = i;
            break;
        }
    }

    for (i = 0; i < ptlr; i++)
    {

        if (ptbr[i].presente)
        {
            if (ptbr[i].tlastaccess < ptbr[last].tlastaccess)
            {
                last = i;
            }
        }
    }

    ptbr[last].presente = 0;
    systemframetable[ptbr[last].framenumber].assigned = 0;
    for (i = systemframetablesize + framesbegin; i < 2*systemframetablesize + framesbegin; i++)
    {
        if (!systemframetable[i].assigned)
        {
            systemframetable[i].assigned = 1;
            if (ptbr[last].modificado)
            {
                char temp[4096];
                readFile((i - framesbegin), temp);
                writeFile((i - framesbegin), systemframetable[ptbr[last].framenumber].paddress);
                writeFile(last, temp);
            }
            ptbr[last].framenumber = i;
            break;
        }
    }
}

void isVirtualFrame(unsigned int frame){
    if ((2*systemframetablesize + framesbegin  > frame) 
        && (frame >= systemframetablesize + framesbegin))
    {
        systemframetable[frame].assigned = 0;
    }
}


int getfreeframe()
{
    int i;
    // Busca un marco libre en el sistema
    for(i=framesbegin;i<systemframetablesize+framesbegin;i++)
        if(!systemframetable[i].assigned)
        {
            systemframetable[i].assigned=1;
            break;
        }
    if(i<systemframetablesize+framesbegin)
        systemframetable[i].assigned=1;
    else
        i=-1;
    return(i);
}

void writeFile(int marcoV, char *buffer)
{
    FILE * file ;
    file = fopen("swap", "r+");
    fseek(file, marcoV*4096, SEEK_SET);
    fwrite(buffer, sizeof(char), 4096, file);
    fclose(file);
}

void readFile(int marcoV, char *buffer)
{
    FILE * file ;
    file = fopen("swap", "r+");
    fseek(file, marcoV*4096, SEEK_SET);
    fread(buffer, sizeof(char), 4096, file);
    fclose(file);
}