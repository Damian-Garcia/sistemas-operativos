#include "scheduler.h"
#include <stdio.h>

extern THANDLER threads[MAXTHREAD];
extern int currthread;
extern int blockevent;
extern int unblockevent;

#define MAXCOLAS 100

// Para este programa se tiene una arreglo de colas de listos, en lugar
// de una sola, y es una cantidad finita de colas
QUEUE ready[MAXCOLAS];
QUEUE waitinginevent[MAXTHREAD];

// Este arrreglo nos permite saber en que cola se encuentra cada proceso
int colaActual[MAXTHREAD + 1];

void scheduler(int arguments)
{
	int old, next;
	int changethread = 0;
	int waitingthread = 0;

	int event = arguments & 0xFF00;
	int callingthread = arguments & 0xFF;

	/*
		En el evento del timer lo que se debe hacer es 
		pasar el thread actual a la siguiente cola de prioridad
		de la que esta actualmente
	*/
	if (event == TIMER)
	{
		changethread = 1;
		threads[callingthread].status = READY;

		//Se calcula la siguiente cola para el proceso actual
		int sig = (colaActual[callingthread] + 1);
		
		// Si se trata del hilo 0, el calendarizador, no tiene caso moverlo
		if (callingthread == 0)
		{
			sig = 0;
		}
		else if (sig >= MAXCOLAS) 
		{ // Si ya estamos e la ultima cola no se debe mover
			sig = MAXCOLAS - 1;
		}
		// Se inserta el proceso en la siguiente cola y se guarda en cual 
		// cola se se inserto
		_enqueue(&ready[sig], callingthread);
		colaActual[callingthread] = sig;
	}

	if (event == NEWTHREAD)
	{
		// Un nuevo hilo va a la cola de listos
		threads[callingthread].status = READY;
		// Los nuevos hilos llegan a la primera cola
		_enqueue(&ready[0], callingthread);
	}

	if (event == BLOCKTHREAD)
	{
		threads[callingthread].status = BLOCKED;
		_enqueue(&waitinginevent[blockevent], callingthread);

		changethread = 1;
	}

	if (event == ENDTHREAD)
	{
		threads[callingthread].status = END;
		changethread = 1;
	}

	if (event == UNBLOCKTHREAD)
	{
		// Cuando el proceso del orquestador se desbloquea debe pasar
		// a la primera cola
		threads[callingthread].status = READY;
		_enqueue(&ready[0], callingthread);
	}

	if (changethread)
	{
		old = currthread;
		int sig; // Entero para guardar el identificador de la cola actual
		if (callingthread == 0)
		{ // Si se trata del calendarizador, se sigue sacando de la misma cola siempre
			sig = colaActual[0];
		}
		int i;
		// Se obtiene la primera cola con procesos pendientes por sacar
		for (i = 0; i < MAXCOLAS; i++)
		{
			if (!_emptyq(&ready[i]))
			{
				sig = i;
				// Ya que la encontramos nos detenemos
				break;
			}
		}

		// Se saca el proceso de la cola encontrada.
		next = _dequeue(&ready[sig]);

		// Se realiza el mismo proceso que en los demas calendarizadores
		threads[next].status = RUNNING;
		_swapthreads(old, next);
	}
}
