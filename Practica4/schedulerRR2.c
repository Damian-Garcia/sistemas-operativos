#include "scheduler.h"

extern THANDLER threads[MAXTHREAD];
extern int currthread;
extern int blockevent;
extern int unblockevent;

QUEUE ready;
QUEUE waitinginevent[MAXTHREAD];

void scheduler(int arguments)
{
	int old, next;
	int changethread = 0;
	int waitingthread = 0;

	int event = arguments & 0xFF00;
	int callingthread = arguments & 0xFF;

	static int cuenta = 0;//contador que se utiliza para ver en que quantum vamos

	//Condicional a la cual se entra cuando ocurre el evento de que pasan 100ms
	if (event == TIMER)
	{
		/*Se incrementa la cuenta en 1 y cuando llegue a 2 se hace un mod para
		 *reiniciar el quantum
		 */
		cuenta++;
		cuenta %= 2;
		//Condicional a la cual se entra cuando cuentaa llega a 2
		if (cuenta == 0)
		{
			changethread = 1;//Se prende la bandera para cambiar de hilo
			//Se manda el hilo actual a la cola de listos
			threads[callingthread].status = READY;
			_enqueue(&ready, callingthread);
		}
	}

	if (event == NEWTHREAD)
	{
		// Un nuevo hilo va a la cola de listos
		threads[callingthread].status = READY;
		_enqueue(&ready, callingthread);
	}

	if (event == BLOCKTHREAD)
	{

		threads[callingthread].status = BLOCKED;
		_enqueue(&waitinginevent[blockevent], callingthread);

		changethread = 1;
	}

	if (event == ENDTHREAD)
	{
		threads[callingthread].status = END;
		changethread = 1;
		//Se reinicia la cuenta para el caso de que un proceso termine antes de
		//ejecutar el segundo proceso
		cuenta = 0;
	}

	if (event == UNBLOCKTHREAD)
	{
		threads[callingthread].status = READY;
		_enqueue(&ready, callingthread);
	}

	if (changethread)
	{
		old = currthread;
		next = _dequeue(&ready);

		threads[next].status = RUNNING;
		_swapthreads(old, next);
	}
}
