

int isinodefree(int inode);
int nextfreeinode();
int assigninode(int inode);
int unassigninode(int inode);


int isblockfree(int block);
int nextfreeblock();
int assignblock(int block);
int unassignblock(int block);
