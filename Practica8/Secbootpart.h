
#pragma pack(2)
// Esta estructura debe ser un sector y debe medir 512 bytes
struct SECBOOTPART {
	char jump[4];						// Instrucción JMP para transferir el ccontrol a la dirección donde está el código de boot del SO
	char nombre_particion[8];
	unsigned short sec_inicpart;		// 0 sectores 
	unsigned char sec_res;		// 1 sector reservado
	unsigned char sec_mapa_bits_area_nodos_i;// 1 sector
	unsigned char sec_mapa_bits_bloques;	// 6 sectores
	unsigned short sec_tabla_nodos_i;	// 3 sectores, directorio raíz
	unsigned int sec_log_particion;		// 43199 sectores
	unsigned char sec_x_bloque;			// 2 sectores por bloque
	unsigned char heads;				// 8 superficies				
	unsigned char cyls;				// 200 cilindros
	unsigned char secfis;				// 27 sectores por track
	char bootcode[484];	// Código de arranque
};