#ifndef OPENFILES_H
#define OPENFILES_H

struct OPENFILES {
	int inuse;		// 0 cerrado, 1 abierto
	unsigned short inode;
	int currpos;
	int currbloqueenmemoria;
	char buffer[1024];
	unsigned short buffindirect[512]; //		
};

typedef int VDDIR;
	
struct vddirent 
{
	char *d_name;
};

#endif