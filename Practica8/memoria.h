#include <stdio.h>
#include "Secbootpart.h"
#include "INode.h"
#include "OpenFiles.h"

struct SECBOOTPART * getSecBoot();

unsigned char * loadBlocksMap(int mapa_bits_bloques);
unsigned char * loadiNodesMap(int mapa_bits_bloques);
struct INODE * loadiNodesTable();
struct OPENFILES * loadOpenFiles();