#include "manejoDiscoLogico.h"
#include "vdisk.h"
#include "memoria.h"


int mapSuperficie(int nseclog){
	struct SECBOOTPART *sbp;
	sbp = getSecBoot();
	int superficie = ((nseclog+1) / sbp->secfis) % sbp->heads ;
	return superficie;
}

int mapCilindro(int nseclog){
	struct SECBOOTPART *sbp;
	sbp = getSecBoot();
	int cilindro = (nseclog+1) / (sbp->secfis * sbp->heads);
	return cilindro;
}

int mapSectorFisico(int nseclog){
	struct SECBOOTPART *sbp;
	sbp = getSecBoot();	
	int sectorFisico = ((nseclog+1) % sbp->secfis) + 1;
	return sectorFisico;
}

int vdreadseclog(int nseclog, char *buffer)
{
	int secfis,cilindro,superficie;
	int unidad=0;
	// Obtener los datos de la partición para saber
	// en que sector físico, cilindro y superficie inicia 
	// la partición
	// Probablemente sea necesario también tener en memoria
	// el sector de boot de la partición para los datos de
	// la geometría de la partición.
	
	//  Calcular secfis,cilindro,superficie a partir de nseclog
	secfis = mapSectorFisico(nseclog);
	cilindro = mapCilindro(nseclog);
	superficie = mapSuperficie(nseclog);

	vdreadsector(unidad,superficie,cilindro,secfis,1,buffer);
}

int vdwriteseclog(int nseclog, char *buffer)
{
	int secfis,cilindro,superficie;
	int unidad=0;
	// Obtener los datos de la partición para saber
	// en que sector físico, cilindro y superficie inicia 
	// la partición
	// Probablemente sea necesario también tener en memoria
	// el sector de boot de la partición para los datos de
	// la geometría de la partición.
	
	//  Calcular secfis,cilindro,superficie a partir de nseclog
	secfis = mapSectorFisico(nseclog);
	cilindro = mapCilindro(nseclog);
	superficie = mapSuperficie(nseclog);
	vdwritesector(unidad,superficie,cilindro,secfis,1,buffer);
}
