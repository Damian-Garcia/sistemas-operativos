#include <stdio.h>

#pragma pack(2)
struct PARTITION 
{
	unsigned char status;
	unsigned char CHS_begin[3];
	unsigned char partition_type;
	unsigned char CHS_end[3];
	unsigned int LBA;
	unsigned int secs_per_partition;
};

struct MBR 
{
	unsigned char bootcode[446];	// 446 +
	struct PARTITION partition[4];	//  64 +
	unsigned short bootsignature;	//   2 = 512
};