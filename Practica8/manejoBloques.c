#include "manejoBloques.h"
#include "memoria.h"
#include "manejoDiscoLogico.h"


int readblock(int nbloque,char *buffer)
{
	int i;
	// Necesito traer a memoria el sector de boot de la partición
	// porque ahí viene el dato de cuántos sectores hay en un bloque
	// Calcular el sector lógico donde comienzan el área de datos (slad)
	
	struct SECBOOTPART * sbp;
	sbp = getSecBoot();

	// Inicio area de datos =  setor de inicio de la partición +
	// 							sectores reservados +
    //							sectores mapa de bits area de nodos i+	
	//							sectores mapa de bits area de datos+	
	//							sectores area nodos i (dr)
	int inicio_area_datos = sbp->sec_inicpart+sbp->sec_res+sbp->sec_mapa_bits_area_nodos_i +sbp->sec_mapa_bits_bloques+sbp->sec_tabla_nodos_i;


	for(i=0;i< sbp->sec_x_bloque;i++)
		vdreadseclog(inicio_area_datos+(nbloque-11)*sbp->sec_x_bloque+i,buffer+512*i);
	
	return(1);	

}

int writeblock(int nbloque,char *buffer)
{
	int result;
	int i;

	struct SECBOOTPART * sbp;
	sbp = getSecBoot();

	// Inicio area de datos =  setor de inicio de la partición +
	// 							sectores reservados +
    //							sectores mapa de bits area de nodos i+	
	//							sectores mapa de bits area de datos+	
	//							sectores area nodos i (dr)
	int inicio_area_datos = sbp->sec_inicpart+sbp->sec_res+sbp->sec_mapa_bits_area_nodos_i +sbp->sec_mapa_bits_bloques+sbp->sec_tabla_nodos_i;


	// Escribir todos los sectores que corresponden al 
	// bloque
	for(i=0;i<sbp->sec_x_bloque;i++)
		vdwriteseclog(inicio_area_datos+(nbloque-1)*sbp->sec_x_bloque+i,buffer+512*i);
	return(1);

}