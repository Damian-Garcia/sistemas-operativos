#include "memoria.h"
#include "vdisk.h"
#include "manejoDiscoLogico.h"

static int secboot_en_memoria;
static int blocksmap_en_memoria;
static int inodesmap_en_memoria;
static int nodos_i_en_memoria;
static int openfiles_inicializada;

static struct SECBOOTPART sbp;

static unsigned char inodesmap[512];
static unsigned char blocksmap[512*6];
static struct INODE inode[24];
static struct OPENFILES openfiles[24];

struct SECBOOTPART * getSecBoot(){
    if(!secboot_en_memoria){
        vdreadsector(0,0,0,1,1,(char *) &sbp);
        secboot_en_memoria = !0;
    }
    return &sbp;
}

unsigned char * loadBlocksMap(int mapa_bits_bloques){
   	int i;
	// Verificar si ya está en memoria, si no, cargarlo
	if(!blocksmap_en_memoria)
	{
		// Cargar todos los sectores que corresponden al 
		// mapa de bits
		for(i=0;i<sbp.sec_mapa_bits_bloques;i++)
		{
			int result=vdreadseclog(mapa_bits_bloques+i,blocksmap+i*512);
		}
		blocksmap_en_memoria=1;
	}
	return blocksmap;
}

unsigned char * loadiNodesMap(int mapa_bits_nodos_i){
	if(!inodesmap_en_memoria)
	{
		// Si no está en memoria, hay que leerlo del disco
		int result=vdreadseclog(mapa_bits_nodos_i,inodesmap);
		inodesmap_en_memoria=1;
	}
}

struct INODE * loadiNodesTable(){
	int i;
	int result;
	int inicio_nodos_i = sbp.sec_res + sbp.sec_tabla_nodos_i + sbp.sec_mapa_bits_bloques;
	if(!nodos_i_en_memoria)
	{
		for(i=0;i<sbp.sec_tabla_nodos_i;i++)
        	result=vdreadseclog(inicio_nodos_i+i,(char *)&inode[i*8]);
        nodos_i_en_memoria=1;
	}	
	return inode;
}

struct OPENFILES * loadOpenFiles(){
	int i;
	if(!openfiles_inicializada)
	{
		// La primera vez que abrimos un archivo, necesitamos
		// inicializar la tabla de archivos abiertos
		for(i=3;i<16;i++)
		{
			openfiles[i].inuse=0;
			openfiles[i].currbloqueenmemoria=-1;
		}
		openfiles_inicializada=1;
	}	
	return openfiles;
}